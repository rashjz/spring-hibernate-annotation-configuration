/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rashjz.info.com.az.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Rashad Javadov
 */
@Entity
@Table(name = "APP_NEWS")
@NamedQueries({
    @NamedQuery(name = "AppNews.findAll", query = "SELECT a FROM AppNews a"),
    @NamedQuery(name = "AppNews.findByRecId", query = "SELECT a FROM AppNews a WHERE a.recId = :recId"),
    @NamedQuery(name = "AppNews.findByTitle", query = "SELECT a FROM AppNews a WHERE a.title = :title"),
    @NamedQuery(name = "AppNews.findByContent", query = "SELECT a FROM AppNews a WHERE a.content = :content"),
    @NamedQuery(name = "AppNews.findByInsDate", query = "SELECT a FROM AppNews a WHERE a.insDate = :insDate"),
    @NamedQuery(name = "AppNews.findByImg", query = "SELECT a FROM AppNews a WHERE a.img = :img"),
    @NamedQuery(name = "AppNews.findByVideo", query = "SELECT a FROM AppNews a WHERE a.video = :video"),
    @NamedQuery(name = "AppNews.findByHitCount", query = "SELECT a FROM AppNews a WHERE a.hitCount = :hitCount"),
    @NamedQuery(name = "AppNews.findByNote", query = "SELECT a FROM AppNews a WHERE a.note = :note"),
    @NamedQuery(name = "AppNews.findByStatus", query = "SELECT a FROM AppNews a WHERE a.status = :status")})
public class AppNews implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "REC_ID")
    private Integer recId;
    @Size(max = 95)
    @Column(name = "TITLE")
    private String title;
    @Size(max = 745)
    @Column(name = "CONTENT")
    private String content;
    @Basic(optional = false)
    @NotNull
    @Column(name = "INS_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date insDate;
    @Size(max = 95)
    @Column(name = "IMG")
    private String img;
    @Size(max = 45)
    @Column(name = "VIDEO")
    private String video;
    @Column(name = "HIT_COUNT")
    private Integer hitCount;
    @Size(max = 45)
    @Column(name = "NOTE")
    private String note;
    @Size(max = 3)
    @Column(name = "STATUS")
    private String status;

    public AppNews() {
    }

    public AppNews(Integer recId) {
        this.recId = recId;
    }

    public AppNews(Integer recId, Date insDate) {
        this.recId = recId;
        this.insDate = insDate;
    }

    public Integer getRecId() {
        return recId;
    }

    public void setRecId(Integer recId) {
        this.recId = recId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getInsDate() {
        return insDate;
    }

    public void setInsDate(Date insDate) {
        this.insDate = insDate;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public Integer getHitCount() {
        return hitCount;
    }

    public void setHitCount(Integer hitCount) {
        this.hitCount = hitCount;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (recId != null ? recId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AppNews)) {
            return false;
        }
        AppNews other = (AppNews) object;
        if ((this.recId == null && other.recId != null) || (this.recId != null && !this.recId.equals(other.recId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "rashjz.info.com.az.entity.AppNews[ recId=" + recId + " ]";
    }

}
